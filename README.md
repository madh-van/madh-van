# Hi there, I'm Madhavan  :wave:

  - :zap: I'm a deep learner; who is addicted to learning and growing
  every day
  - :earth_africa: I am currently sharing a little bit of my knowledge
    to the world through [my blogs](https://madh-van.gitlab.io/blog/)

 Below are the areas I am passionate about;

  - **Artificial Intelligence**
    - Design, Protyping, Test - ML Models *(across its life-cycle)* using
      Mlflow, Tensorflow, Pytorch, Sklearn, Keras
    - Exploratory Data Analysis - Data modeling - Training -
      Validation - Deployment
    - Image Processing, Computer Vision, Machine Learning, Deep
      Learning, Reinforcement Learning.
  - **SW development**
    - Follow best practices like Agile Scrum and Test Driven Development.
    - Love Abstractions whilst building Frameworks and Tools.
    - Automation with CI/CD pipelines (Jenkins).
  - **GNU/Linux Systems**
    - Proponent of free software and user freedom. 
    - Emacs evangelist.
    - Guix system (Reproducable environment).
